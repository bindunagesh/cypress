import * as biDictionaryLocators from "../locators/biDictionary_locators"
import * as settinglocators from "../locators/setting_locators"
import { logToGlobalFile } from '../utilities/global_log'




export function verifyAuthorizedUsersInBiDi(fields,reportName,authorizedUser){
    logToGlobalFile("Wait: 2000 milliseconds")
    cy.wait(2000)
    logToGlobalFile(`Typed "${fields}" in the search field on the first page`)
    cy.get(biDictionaryLocators.table.searchFirstPage).type(fields)
    logToGlobalFile("Wait: for 1000 milliseconds")
    cy.wait(1000)
    logToGlobalFile(`Verified that the search card contains "${fields}"`)
    cy.get(settinglocators.adminSettings.applicationSettings.thumbnailSetting.searchcard).contains(fields)
    logToGlobalFile("Clicked on the action column")
    cy.get(biDictionaryLocators.table.actionColoumn).eq(1).click()   
    //search for report.
    logToGlobalFile("Clicked on the search field on the first page")
    cy.get(biDictionaryLocators.table.searchFirstPage).click()
    logToGlobalFile(`Typed "${reportName}" in the search field on the second page`)
    cy.get(biDictionaryLocators.table.searchSecondPage).type(reportName)
    logToGlobalFile(`Verified that the search card contains "${reportName}"`)
    cy.get(settinglocators.adminSettings.applicationSettings.thumbnailSetting.searchcard).contains(reportName)
    logToGlobalFile("Wait: 1000 milliseconds")
    cy.wait(1000)
    //search for Authorized Users
    //cy.get(biDictionaryLocators.table.authorizedUserButton).click()
    logToGlobalFile("Clicked on the authorized user button")
    cy.get(biDictionaryLocators.table.authorizedUserButton).eq(0).click() //added
    logToGlobalFile("Clicked on the pagination drop-down")
    cy.get(biDictionaryLocators.table.PaginationDropDown).click()
    
    cy.get(biDictionaryLocators.table.PaginationDropDownOptions).contains('100').click()
    logToGlobalFile(` Verified that the authorized user list has ${authorizedUser} items`)
    cy.get(biDictionaryLocators.table.AuthorizedUserList).should('have.length', authorizedUser)
}

export function searchBiDictinaryField(field){
    logToGlobalFile(` Typed "${field}" in the search field on the first page`)
    cy.get(biDictionaryLocators.table.searchFirstPage).type(field)
    logToGlobalFile(` Verified that the search card contains "${field}"`)
    cy.get(settinglocators.adminSettings.applicationSettings.thumbnailSetting.searchcard).contains(field)
    
}

export function approvefield(){
    
    cy.get(biDictionaryLocators.table.actionColoumn).eq(0).click() 
    logToGlobalFile("Clicked on the approved checkbox")
    cy.get(biDictionaryLocators.editRecord.approvedCheckbox).click()
    logToGlobalFile("Clicking on the update button")
    cy.get(biDictionaryLocators.editRecord.updateButton).click()

}


        