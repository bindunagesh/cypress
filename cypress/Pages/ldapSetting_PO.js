import * as settingLocators from "../locators/setting_locators"
//const applicationSettingUrl = "https://win-trial.zenoptics.com:8443/com.zenoptics.services/#/admin/applicationsettings"
//const applicationSettingUrl='http://trial.zenoptics.com:9100/com.zenoptics.services/#/admin/applicationsettings'
//const applicationSettingUrl='http://upgrade-test.zenoptics.com:9100/com.zenoptics.services/#/admin/applicationsettings'
const applicationSettingUrl = "https://qa2.zenoptics.com:8443/com.zenoptics.services/#/admin/applicationsettings";

export function openldapConfigurationTab(){
    
    cy.get(settingLocators.adminSettings.applicationSettings.ldapConfiguaration.tab).click()
}

export function ldapCheckbox(){
    cy.get(settingLocators.adminSettings.applicationSettings.ldapConfiguaration.ldapCheckbox).invoke('attr', 'aria-checked').then(ldapChecked => {
        if (ldapChecked === "false")
        {
           cy.get(settingLocators.adminSettings.applicationSettings.ldapConfiguaration.ldapClickCheckbox).click()
          
       }
       else{
           cy.log('ldap checkbox is already checked')
       }
    });  
}


export function ldapWithOneUrl(ldapurl1){

    ldapCheckbox()
    cy.get(settingLocators.adminSettings.applicationSettings.ldapConfiguaration.ldapServerUrl1).clear().type(ldapurl1)
    cy.get(settingLocators.adminSettings.applicationSettings.ldapConfiguaration.testConnection).eq(0).click()
    cy.get(settingLocators.adminSettings.applicationSettings.ldapConfiguaration.ldapConfigParent).then(($ele) => {
        if ($ele.find(settingLocators.adminSettings.applicationSettings.ldapConfiguaration.removeLdapUrl).length > 0) {
            cy.get(settingLocators.adminSettings.applicationSettings.ldapConfiguaration.removeLdapUrl).click()

        } else {
            cy.log('remove ldap url not present')
        }
    })    
}

export function ldapWithTwoUrl(ldapurl1,ldapurl2){
    openldapConfigurationTab()
    ldapCheckbox()
    cy.get(settingLocators.adminSettings.applicationSettings.ldapConfiguaration.ldapServerUrl1).clear().type(ldapurl1)
    cy.get(settingLocators.adminSettings.applicationSettings.ldapConfiguaration.testConnection).eq(0).click()
    cy.get(settingLocators.adminSettings.applicationSettings.ldapConfiguaration.ldapConfigParent).then(($ele) => {
        if ($ele.find(settingLocators.adminSettings.applicationSettings.ldapConfiguaration.removeLdapUrl).length > 0) {
            cy.get(settingLocators.adminSettings.applicationSettings.ldapConfiguaration.removeLdapUrl).click()
            cy.get(settingLocators.adminSettings.applicationSettings.ldapConfiguaration.addAdditionalLdapurl).click()
            cy.get(settingLocators.adminSettings.applicationSettings.ldapConfiguaration.ldapServerUrl2).clear().type(ldapurl2)
            cy.get(settingLocators.adminSettings.applicationSettings.ldapConfiguaration.testConnection).eq(1).click()

        } else {
            cy.log('remove ldap url not present')
            cy.get(settingLocators.adminSettings.applicationSettings.ldapConfiguaration.addAdditionalLdapurl).click()
            cy.get(settingLocators.adminSettings.applicationSettings.ldapConfiguaration.ldapServerUrl2).clear().type(ldapurl2)
            cy.get(settingLocators.adminSettings.applicationSettings.ldapConfiguaration.testConnection).eq(1).click()
        }
    })    
}

export function ldapBindDN(ldapBindDN){
 cy.get(settingLocators.adminSettings.applicationSettings.ldapConfiguaration.ldapbinddn).clear({force:true}).wait(15000).type(ldapBindDN) 
}

export function ldapPassword(ldapPassword){
    cy.get(settingLocators.adminSettings.applicationSettings.ldapConfiguaration.ldappassword).clear({force:true}).wait(1500).type(ldapPassword) 
   }

export function ldapUserBase(ldapUserBase){
    cy.get(settingLocators.adminSettings.applicationSettings.ldapConfiguaration.ldapuserbase).clear({force:true}).type(ldapUserBase) 
   }

export function ldapAttributes(ldapAttributes){
    cy.get(settingLocators.adminSettings.applicationSettings.ldapConfiguaration.ldapattributes).clear({force:true}).type(ldapAttributes) 
   }

export function updateButton(){
    cy.get(settingLocators.adminSettings.applicationSettings.ldapConfiguaration.updateButton).wait(15000).click({force:true}) 
    cy.get('[id="toast-container"] div[aria-label="LDAP configured successfully"]').should('exist')
   }
     