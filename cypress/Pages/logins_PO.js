import * as login from "../locators/login_locators"

export function loginUser2(Url,userName,password) {
    cy.visit(Url);
    cy.get(login.usn_input).type(userName);
    cy.get(login.pwd_input).type(password);
    cy.get(login.submit_btn).click();
}