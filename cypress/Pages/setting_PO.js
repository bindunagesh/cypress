import settingLocators, * as settinglocators from "../locators/setting_locators"
import * as dashboard from "../Pages/dashboard_PO";
import { faker } from '@faker-js/faker';
import { logToGlobalFile } from '../utilities/global_log'





export function openThumbnailSettingTab(){
    logToGlobalFile("Clicking on Administrator Settings");
    cy.get(settinglocators.adminSettings.administratorSettings).contains('Administrator Settings').click()
    logToGlobalFile("Clicking on Application Settings");
    cy.get(settinglocators.adminSettings.applicationSettings.getApplicationSetting).contains('Application Settings').click()
    logToGlobalFile("Clicking on Thumbnail Settings");
    cy.get(settinglocators.adminSettings.applicationSettings.thumbnailSetting.tab).eq(5).click()

}

export function openLdapConfigurationTab(){
    cy.get(settinglocators.adminSettings.administratorSettings).contains('Administrator Settings').click()
    cy.get(settinglocators.adminSettings.applicationSettings.getApplicationSetting).contains('Application Settings').click()
    dashboard.openMenu()
    cy.get(settinglocators.adminSettings.applicationSettings.ldapConfiguaration.tab).eq(1).click({force:true})

}

export function thumbnailSearchReport(reportName){
    logToGlobalFile(`Typed ${reportName} in the search input for thumbnail settings`);
    return cy.get(settinglocators.adminSettings.applicationSettings.thumbnailSetting.search).type(reportName)
    

}
export function changeCompanyLogo(CHANGE_COMPANY_LOGO_FILENAME){
    cy.fixture(CHANGE_COMPANY_LOGO_FILENAME, { encoding: null }).as('myFixture')
    cy.get(settinglocators.adminSettings.applicationSettings.uploadCompanyLogo).selectFile('@myFixture', {force: true})
    cy.get(settinglocators.adminSettings.applicationSettings.uploadCompanyLogoClass).should('exist')
    cy.get(settinglocators.adminSettings.applicationSettings.uploadCompanyLogoButton).contains('UPLOAD').click()
    cy.get(settinglocators.adminSettings.applicationSettings.uploadCompanyLogoToastNotification).contains(' Logo Uploaded successfully ')
    

}

export function thumbnailVerifySearch(reportName){
    logToGlobalFile('Verified that the search card has the expected text');
    return cy.get(settinglocators.adminSettings.applicationSettings.thumbnailSetting.searchcardReportName).eq(0).should('have.text',reportName) 

}

export function UploadThumbnail(path){
    logToGlobalFile(`Selected file at path: ${path}`);
    cy.get(settinglocators.adminSettings.applicationSettings.thumbnailSetting.chooseThumbnailIcon).invoke('show').eq(0).selectFile(path)
    logToGlobalFile('Clicked on "Upload Thumbnail Images" button') 
    cy.get(settinglocators.adminSettings.applicationSettings.thumbnailSetting.uploadbutton).contains("Upload Thumbnail Images").and('be.enabled').click()
    logToGlobalFile('Verified that the state column contains "Admin Uploaded"');
    cy.get(settinglocators.adminSettings.applicationSettings.thumbnailSetting.stateColumn).contains('Admin Uploaded')
    
}

export function deleteUploadedThumbnail(){
    cy.get(settinglocators.adminSettings.applicationSettings.thumbnailSetting.deleteUploadedThumbnail).click()
    cy.get(settinglocators.adminSettings.applicationSettings.thumbnailSetting.thumbnailDeleteConfirmation).click()
    
    
}

export function generateThumbnail(){
    cy.get(settinglocators.adminSettings.applicationSettings.thumbnailSetting.checkbox).click()
    cy.get(settinglocators.adminSettings.applicationSettings.thumbnailSetting.uploadbutton).contains('Generate Thumbnails').click()
    cy.get(settinglocators.adminSettings.applicationSettings.thumbnailSetting.statusColumn).should("have.text",' waiting ')

}

export function openBiDictionary(){
    logToGlobalFile('Clicking on Steward Manegement');
    cy.get(settinglocators.adminSettings.administratorSettings).contains('Steward Management').click()
    logToGlobalFile('Clicking on Bi Dictionary');
    cy.get(settinglocators.adminSettings.applicationSettings.getApplicationSetting).contains('BI Dictionary').click()
    
   

}
//added
export function openBiDictionaryReload(){
    logToGlobalFile('Clicking on Steward Manegement');
    cy.get(settinglocators.adminSettings.administratorSettings).contains('Steward Management').click()
    logToGlobalFile('Clicking on Bi Dictionary') 
    cy.get(settinglocators.adminSettings.applicationSettings.getApplicationSetting).contains('BI Dictionary').click()
    logToGlobalFile('Click back');
    cy.go('back')
    logToGlobalFile('Reload: page Reload');
    cy.reload()
    
   

}
export function openApplicationSettings(){
    cy.get(settinglocators.adminSettings.administratorSettings).contains('Administrator Settings').click()
    cy.get(settinglocators.adminSettings.applicationSettings.getApplicationSetting).contains('Application Settings').click()
    cy.get('body').click()
    
   

}
export function openSourceConnectionDetails(){
    logToGlobalFile('Clicking on Administrator Settings');
    cy.get(settinglocators.adminSettings.administratorSettings).contains('Administrator Settings').click()
    logToGlobalFile('Clicking on Source Connection settings')
    cy.get(settinglocators.adminSettings.applicationSettings.getsourceconnectionsetting).click()
    logToGlobalFile('Clicking on body')
    cy.get('body').click()
    
   

}






export function clickOnNextUser(){
let userId1_user;
let userId2_user;

  // nextbutton
  cy.get(settinglocators.adminSettings.userid).eq(0).invoke('text').then((text1) => {
    userId1_user = text1.trim();
    expect(userId1_user).to.equal(userId1_user);
  });
      
    cy.get(settinglocators.adminSettings.PreviousNextButton).eq(1).click()    
    //previousbutton
    cy.wait(2000)
    cy.get(settinglocators.adminSettings.userid).eq(0).invoke('text').then((text1) => {
        userId2_user = text1.trim();
        expect(userId2_user).to.equal(userId2_user);
      });

    cy.get(settinglocators.adminSettings.PreviousNextButton).eq(0).click()

}

export function clickOnNextSourceConnection() {
    let userId1_source;
    let userId2_source;
 
    cy.get(settinglocators.adminSettings.connectionnameid).eq(0).invoke('text').then((text1) => {
      userId1_source = text1.trim();
      expect(userId1_source).to.equal(userId1_source);
    });
  
    cy.get(settinglocators.adminSettings.PreviousNextButton).eq(1).click();
    cy.wait(2000);
    cy.get(settinglocators.adminSettings.connectionnameid).eq(0).invoke('text').then((text2) => {
      userId2_source = text2.trim();
      expect(userId2_source).to.equal(userId2_source);
    });
    cy.get(settinglocators.adminSettings.PreviousNextButton).eq(0).click();
  }



    export function clickOnNextMaintainReports(){
        let userId1_maintain;
        let userId2_maintain;
        
          // nextbutton
          cy.get(settinglocators.adminSettings.reportnameid).eq(0).invoke('text').then((text1) => {
            userId1_maintain = text1.trim();
            expect(userId1_maintain).to.equal(userId1_maintain);
          });
              
            cy.get(settinglocators.adminSettings.PreviousNextButton).eq(1).click()    
            //previousbutton
            cy.wait(2000)
            cy.get(settinglocators.adminSettings.reportnameid).eq(0).invoke('text').then((text1) => {
                userId2_maintain = text1.trim();
                expect(userId2_maintain).to.equal(userId2_maintain);
              });
        
            cy.get(settinglocators.adminSettings.PreviousNextButton).eq(0).click()
        
        }

export function clickOnNextReportTypes(){
        let userId1_report;
        let userId2_report;
            
              // nextbutton
            cy.get(settinglocators.adminSettings.displayvalueid).eq(0).invoke('text').then((text1) => {
            userId1_report = text1.trim();
            expect(userId1_report).to.equal(userId1_report);
          });
                  
            cy.get(settinglocators.adminSettings.PreviousNextButton).eq(1).click()    
            //previousbutton
            cy.wait(2000)
            cy.get(settinglocators.adminSettings.displayvalueid).eq(0).invoke('text').then((text1) => {
            userId2_report = text1.trim();
            expect(userId2_report).to.equal(userId2_report);
          });
            
        cy.get(settinglocators.adminSettings.PreviousNextButton).eq(0).click()
            
    }

    

export function openUsersettings(){
    cy.get(settinglocators.adminSettings.administratorSettings).contains('Administrator Settings').click()
    cy.get(settinglocators.adminSettings.applicationSettings.getApplicationSetting).contains('Users').click()
    cy.get('body').click()
    
   

}

export function openMaintanReport(){
    cy.get(settinglocators.adminSettings.administratorSettings).contains('Administrator Settings').click()
    cy.get(settinglocators.adminSettings.applicationSettings.getApplicationSetting).contains('Maintain Reports').click()
    cy.get('body').click()
    
   

}


export function openMaintanReport_srv15(){
    cy.get(settinglocators.adminSettings.administratorSettings).contains('Steward Management').click()
    cy.get(settinglocators.adminSettings.applicationSettings.getApplicationSetting).contains('Maintain URL Reports').click()
    cy.get('body').click()
    
   

}

export function openJobHistory() {
    cy.get(settinglocators.adminSettings.ConnectionName).eq(1).trigger('mouseover', {force:true})
    cy.get(settinglocators.adminSettings.JobHistory).eq(0).click()
}


export function categorizedReportPage(){
    logToGlobalFile('Clicking on Administrator Settings');
    //cy.get(settinglocators.adminSettings.administratorSettings).contains('Administrator Settings').click()
    logToGlobalFile('Clicking on Steward Management');
    cy.get(settinglocators.adminSettings.administratorSettings).contains('Steward Management').click()
    logToGlobalFile('Clicking on Categorized Reports');
    cy.get(settinglocators.adminSettings.applicationSettings.getApplicationSetting).contains('Categorized Reports').click()
    

}

export function categorizedReportPage_new(){
    dashboard.openMenu()
    dashboard.openMenu()
    logToGlobalFile('Clicking on Steward Management');
    cy.get(settinglocators.adminSettings.administratorSettings).contains('Steward Management').click()
    logToGlobalFile('Clicking on Categorized Report');
    cy.get(settinglocators.adminSettings.applicationSettings.getApplicationSetting).contains('Categorized Reports').click()
    

}

export function openReportTypes(){
    logToGlobalFile('Clicking on Administrator Settings');
    cy.get(settinglocators.adminSettings.administratorSettings).contains('Administrator Settings').click()
    logToGlobalFile('Clicking on Report type testing');
    cy.get(settinglocators.adminSettings.reportTypes.reporttypesetting).click()
    logToGlobalFile('Clicking on page body');
    cy.get('body').click()
    
   

}

export function createActiveUser(firstName,password,lastName,email){
    cy.get(settinglocators.adminSettings.userSettings.addUserButton).click()
    cy.get(settinglocators.adminSettings.userSettings.userIdInput).click().type(firstName)
    cy.get(settinglocators.adminSettings.userSettings.userPassInput).type(password)
    cy.get(settinglocators.adminSettings.userSettings.userPassConfirmInput).type(password)
    cy.get(settinglocators.adminSettings.userSettings.userFirstNameInput).type(firstName)
    cy.get(settinglocators.adminSettings.userSettings.userLastNameInput).type(lastName)
    cy.get(settinglocators.adminSettings.userSettings.userEmailInput).type(email)
    cy.get(settinglocators.adminSettings.userSettings.savebutton).click()
    cy.get(settinglocators.adminSettings.userSettings.Notification).should('exist').and('have.text', ' User details added successfully ')
    
    

}
export function openMyReports(){
    cy.get(settinglocators.adminSettings.administratorSettings).contains('Reports').click()
    cy.get(settinglocators.adminSettings.applicationSettings.getApplicationSetting).contains('My Reports').click()
    
   

}

export function searchconnection(){
   logToGlobalFile('Typing "Tableau" in the search field for report types');
    cy.get(settinglocators.adminSettings.reportTypes.searchReports).type('Tableau')
    
    
   

}

export function editReportType(){
    logToGlobalFile('Clicking on edit row');
    cy.get(settinglocators.adminSettings.reportTypes.editRow).click({force:true})
    logToGlobalFile('Wait: 1500 milliseconds');
    cy.wait(1500)
    
  
}

export function reportUrl(connectionName,launchUrl,thumbnailUrl){
    logToGlobalFile('Editing launch URL and thumbnail URL for Report Type');
    cy.get(settinglocators.adminSettings.reportTypes.checkBox).contains(connectionName).click()
    .parent()
    .parent()
    .parent()
    .children().eq(1).clear().type(launchUrl)
    .next().clear().type(thumbnailUrl)
    //cy.get(settinglocators.adminSettings.reportTypes.checkBox).contains(connectionName).click().type(launchUrl)
    //.next().type(thumbnailUrl)
    logToGlobalFile('Clicking on the submit button to save changes');
    cy.get(settinglocators.adminSettings.reportTypes.submitButton).eq(0).click()
    logToGlobalFile('Verifying success message for Report Type edit');
    cy.get('[id="toast-container"] div[aria-label="Report Type is edited successfully"]').should('exist')
   

}

export function ReportTypecheckbox(connectionName){
    cy.get(settinglocators.adminSettings.reportTypes.checkBox).contains(connectionName).click()
    
}








export function deletethumbnailGenerateNew(){
    cy.get(settinglocators.adminSettings.applicationSettings.thumbnailSetting.actionColumn).then(($body)=>{
     if ($body.find('mat-icon').length===3){
        cy.wrap($body)
        .find('[svgicon="remove-manual-thumbnail"]').click()
        cy.get(settinglocators.adminSettings.applicationSettings.thumbnailSetting.thumbnailDeleteConfirmation).click()
        } 
        generateThumbnail()
    })
    

  

}

export function openCatagoriesPage(){
    cy.get(settinglocators.adminSettings.administratorSettings).contains('Categories').click()
}

export function changetoRG(){
    logToGlobalFile('Change to RG: Opening menu');
    dashboard.openMenu()
    logToGlobalFile('Change to RG: Opening application settings');
    openApplicationSettings()
    logToGlobalFile('Change to RG: Clicking custom attributes');
    cy.get(settinglocators.adminSettings.applicationSettings.customattributes).click()
    cy.wait(2000)
    logToGlobalFile('Change to RG: Toggling One Click switch');
    cy.get(settinglocators.adminSettings.applicationSettings.toggleRG).click()
    cy.wait(2000)
    logToGlobalFile('Change to RG: Toggling RG switch');
    cy.get(settinglocators.adminSettings.applicationSettings.toggleRG).click()
    cy.wait(3000)
    logToGlobalFile('Change to RG: Clicking update button');
    cy.get(settinglocators.adminSettings.applicationSettings.updatebtn).click()

    dashboard.clickHomeIcon()
}

export function openRGContentList(){
    cy.get(settinglocators.adminSettings.administratorSettings).contains('Report Governance').click()
    cy.get(settinglocators.adminSettings.applicationSettings.getApplicationSetting).contains('Content List').click()
}

export function openRGMyTasks(){
    cy.get(settinglocators.adminSettings.administratorSettings).contains('Report Governance').click()
    cy.get(settinglocators.adminSettings.applicationSettings.getApplicationSetting).contains('My Tasks').click()
}

export function clickcustomAttribute_off(){
    cy.get(settinglocators.adminSettings.applicationSettings.thumbnailattributes).eq(4).click()
    cy.get(settinglocators.adminSettings.applicationSettings.togglethumbnailattr).eq(0).click()
    cy.get(settinglocators.adminSettings.applicationSettings.togglethumbnailattr).eq(1).click()
    cy.get(settinglocators.adminSettings.applicationSettings.togglethumbnailattr).eq(2).click()
    cy.get(settinglocators.adminSettings.applicationSettings.thumbnailupdatebtn).click()
    cy.get(settinglocators.adminSettings.applicationSettings.toastmsg).should('be.visible')

}

export function clickcustomAttribute_on(){
    cy.get(settinglocators.adminSettings.applicationSettings.thumbnailattributes).eq(4).click()
    cy.get(settinglocators.adminSettings.applicationSettings.togglethumbnailattr).eq(0).click()
    // Check if the pop-up is present
    //cy.get('app-user-notification').then(($popup) => {
      //  if ($popup.text().includes('Possible attribute redundancy')) {
        // Click 'Yes' if the pop-up contains 'app-user-notification'
       // cy.get('.mat-button-wrapper').eq(1).click({force: true})
       // }
    //})
    cy.get(settinglocators.adminSettings.applicationSettings.togglethumbnailattr).eq(1).click()
    cy.get(settinglocators.adminSettings.applicationSettings.togglethumbnailattr).eq(2).click()
    //cy.get('app-user-notification').then(($popup) => {
    //    if ($popup.text().includes('Possible attribute redundancy')) {
        // Click 'Yes' if the pop-up contains 'app-user-notification'
      //  cy.get('.mat-button-wrapper').eq(1).click({force:true})
        //}
    ///})
    cy.get(settinglocators.adminSettings.applicationSettings.thumbnailupdatebtn).click()
    cy.get(settinglocators.adminSettings.applicationSettings.toastmsg).should('be.visible')

}

export function experimentalflag_modern() {
    cy.get(settinglocators.adminSettings.applicationSettings.systemflag).eq(0).click()
    cy.wait(3000)
    cy.get(settinglocators.adminSettings.applicationSettings.styleswitch).eq(0).click()
    cy.get(settinglocators.adminSettings.applicationSettings.style).eq(1).click()
    cy.get(settinglocators.adminSettings.applicationSettings.toaststyle).should('be.visible')
    cy.get(settinglocators.adminSettings.applicationSettings.closestyle).click()

}
export function experimentalflag_classic() {
    cy.get(settinglocators.adminSettings.applicationSettings.systemflag).eq(0).click()
    cy.wait(3000)
    cy.get(settinglocators.adminSettings.applicationSettings.styleswitch).eq(0).click()
    cy.get(settinglocators.adminSettings.applicationSettings.style).eq(0).click()
    cy.get(settinglocators.adminSettings.applicationSettings.toaststyle).should('be.visible')
    cy.get(settinglocators.adminSettings.applicationSettings.closestyle).click()

}

export function styleswitch_disable() {
    cy.get(settinglocators.adminSettings.applicationSettings.systemflag).eq(0).click()
    cy.wait(3000)
    cy.get(settinglocators.adminSettings.applicationSettings.styleswitch).eq(1).click()
    cy.get(settinglocators.adminSettings.applicationSettings.dashstyle).eq(1).click()
    cy.get(settinglocators.adminSettings.applicationSettings.toaststyle).should('be.visible')
    cy.get(settinglocators.adminSettings.applicationSettings.closestyle).click()
    cy.wait(5000)
    //cy.reload()
    //cy.wait(7000)

}

export function styleswitch_enable() {
    cy.get(settinglocators.adminSettings.applicationSettings.systemflag).eq(0).click()
    cy.wait(3000)
    cy.get(settinglocators.adminSettings.applicationSettings.styleswitch).eq(1).click()
    cy.get(settinglocators.adminSettings.applicationSettings.dashstyle).eq(0).click()
    cy.get(settinglocators.adminSettings.applicationSettings.toaststyle).should('be.visible')
    cy.get(settinglocators.adminSettings.applicationSettings.closestyle).click()
    cy.wait(5000)
    //cy.reload()
    //.wait(7000)

}


export function reportAbout() {
    cy.get(settinglocators.adminSettings.getReportAbout).click()
}

export function openReportFromMyReport(reportname){
    cy.wait(3000)
    //cy.get('#Grid-inactive').click()       //for grid view
    //cy.wait(2000)
    cy.get(settinglocators.adminSettings.applicationSettings.reportOpen.searchReportInMyReport).contains(reportname).click()
    cy.wait(9000)
    cy.get("span[class='ng-star-inserted']").eq(4).click({force:true})
    cy.get("span[class='label']").eq(0).should('exist')
    cy.get("span[class='label']").eq(1).should('exist')
    cy.get("span[class='label']").eq(2).should('exist')
    cy.get("span[class='label']").eq(3).should('exist') 
    cy.get("span[class='label']").eq(4).should('exist')
    cy.get("span[class='label']").eq(5).should('exist')
    cy.get("span[class='label']").eq(6).should('exist')
    cy.get("span[class='label']").eq(7).should('exist')

}

export function navigateReportType(){
    cy.get("span[class='mat-list-item-content']").eq(5).click()
}

export function navigatecategories(){
    cy.get("span[class='mat-list-item-content']").eq(3).click()
}

export function openReportFromReportTypes(connectionName,reportname){
    cy.get('.zo-main-reportlist-container').contains(connectionName).click()
    cy.wait(2000)
    //cy.get('#Grid-inactive').click()       //for grid view
    //cy.wait(2000)
    //cy.get('#mat-select-value-5').click()
    //cy.get('#mat-option-9').click()
    cy.get('.zo-report-grid-container').contains(reportname).click()
    cy.wait(2000)
    cy.get("span[class='ng-star-inserted']").eq(4).click({force:true})
    cy.get("span[class='label']").eq(0).should('exist')
    cy.get("span[class='label']").eq(1).should('exist')
    cy.get("span[class='label']").eq(2).should('exist')
    cy.get("span[class='label']").eq(3).should('exist') 
    cy.get("span[class='label']").eq(4).should('exist')
    cy.get("span[class='label']").eq(5).should('exist')
    cy.get("span[class='label']").eq(6).should('exist')
    cy.get("span[class='label']").eq(7).should('exist')
    
}

export function openReportFromCatPage(cat,subcat,subcat1,rep){
    cy.get('.zo-grid-container').contains(cat).click()
    cy.get('.zo-grid-container').contains(subcat).click()
    //cy.get('#mat-select-value-11').click()
    //cy.get('#mat-option-17').click()
    cy.get('.zo-grid-container').contains(subcat1).click()
    cy.wait(2000)
 
    //cy.get('#Grid-inactive').click()       //for grid view
    //cy.wait(2000)
    cy.get('.zo-report-grid-container').contains(rep).click()
    //cy.get('.zo-unset-height > section.ng-star-inserted').contains(rep).click()
 
    //reportAbout()
    cy.get("span[class='ng-star-inserted']").eq(4).click({force:true})
    cy.get("span[class='label']").eq(0).should('exist')
    cy.get("span[class='label']").eq(1).should('exist')
    cy.get("span[class='label']").eq(2).should('exist')
    cy.get("span[class='label']").eq(3).should('exist') 
    cy.get("span[class='label']").eq(4).should('exist')
    cy.get("span[class='label']").eq(5).should('exist')
    cy.get("span[class='label']").eq(6).should('exist')
    cy.get("span[class='label']").eq(7).should('exist')
    cy.get('.report-title-container').contains(rep)

   
}

export function companyLogo(){
    cy.get('.company-logo-image').should('have.attr','src') //.should('exist').and('include','.svg')
    .then((src) => {
        // Assert that the 'src' attribute contains the expected file extensions
        expect(src).to.match(/\.(png|svg|jpg)$/);
      });
    }
