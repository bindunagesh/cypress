import * as reportLocators from "../locators/report_locators"
import * as login from "../locators/login_locators"
export function checkEmail(subject) {
    let email;
    cy.task("gmail:check", {
      from: "system-notifications@zenoptics.com",
      to: "bindu@zenoptics.com",
      subject: subject,
    })
    .then((result) => {
      email = result;
      cy.log(`Number of emails found: ${email.length}`)
      assert.isNotNull(email, `Email was found`);
      assert.isAtLeast(
        email.length,
        1,
        "Expected to find at least one email, but none were found!"
      );
      cy.reload()
      // let body = email[0].body.html
      // let date = email[0].body.date
      // cy.document({ log: false }).invoke({ log: false }, 'write', body);
      // let link = body.match(/Report Link: (.*?)\r\n/)[1];
      // cy.visit(link);
      // cy.get(login.usn_input).type('hruser');
      // cy.get(login.pwd_input).type('password1234');
      // cy.get(login.submit_btn).click();
      // cy.get(reportLocators.report.reportTitle).contains(subject)
    })
  }

  export function checkEmailSubscribe(subject) {
    let email;
    cy.task("gmail:check", {
      from: "system-notifications@zenoptics.com",
      to: "bindu@zenoptics.com",
      subject: subject,
    })
    .then((result) => {
      email = result;
      cy.log(`Number of emails found: ${email.length}`)
      assert.isNotNull(email, `Email was found`);
      assert.isAtLeast(
        email.length,
        1,
        "Expected to find at least one email, but none were found!"
      );
      let body = email[0].body.html
      let date = email[0].body.date
      cy.document({ log: false }).invoke({ log: false }, 'write', body);
      // let link = body.match(/^https?:(.*?)\r\n/);
      // cy.log(link)
      // cy.visit(link);
      // cy.get(reportLocators.report.reportTitle).contains(subject)
    })
  }
