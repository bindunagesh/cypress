const favoriteLocators ={

    sort: '.zo-sort > :nth-child(2)',
    newestFirst:':nth-child(1) > .inactive-item > :nth-child(1)',
    reportName:':nth-child(1) > app-grid-card > .zo-report-grid-details > .zo-report-data-section > .zo-report-grid-head > .zo-report-grid-head-name > .mat-tooltip-trigger'
  }
  module.exports =favoriteLocators