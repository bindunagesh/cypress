/// <reference types="Cypress" />

import { randomData } from  '../Pages/fakerData'
import data from "../fixtures/sanityData.json"
import * as sourceConnection from "../fixtures/sourceConnection.json"
import * as source from "../Pages/sourcesystem_PO"  
import navbarlinks from "../fixtures/leftnavbarlinks.json";
import navbarreportlinks from "../fixtures/leftnavbarreportlinks.json";
import dashboardLinks from "../fixtures/dashboard_links.json";
import * as dashboard from "../Pages/dashboard_PO";
import * as favorite from "../Pages/favorite_PO"
import * as gobal from "../Pages/gobal_PO";
import * as setting from "../Pages/setting_PO";
import * as report from "../Pages/report_PO";
import * as biDictionary from "../Pages/biDictonary_PO";
import { checkEmail } from "../utilities/email";
import { checkEmailSubscribe } from "../utilities/email"
import * as reportType from "../Pages/reportType_PO";
import * as categories from "../Pages/categories_PO";
import * as biGlossary from '../Pages/biGlossary_PO'
import * as logins from'../Pages/logins_PO'
import * as categorizedReportPage from '../Pages/categorized_reports_PO'
import * as ldap from "../Pages/ldapSetting_PO"
import * as ldapConfiguaration from "../fixtures/ldapConfiguration.json"
import * as reportRating from "../fixtures/report.json"
import * as reportStatistics from "../Pages/reportStatistics_PO"
import { createWorkflow_multipleuser } from '../Pages/report_PO'
import { logToGlobalFile, logScreenshots } from '../utilities/global_log'




  
describe("Smoke Suite", () => {
  beforeEach(() => {
    logToGlobalFile('Executing beforeEach hook');
    logins.loginUser2(Cypress.env(),Cypress.env('adminUserName'),Cypress.env('adminPassword'));
    
   
  });
  
  //added
it("TC-00001_Verify creating a new Tableau Connection", () => {
   logToGlobalFile("TC-00001_Verify creating a new Tableau Connection")
    dashboard.openMenu()
    setting.openSourceConnectionDetails()
    logToGlobalFile('Adding details to creatte Test Connection')
    source.createTestConnection(
    
      sourceConnection.Tableau_c8.systemName,
      sourceConnection.Tableau_c8.connectionName,
      sourceConnection.Tableau_c8.tabDBConn, 
      sourceConnection.Tableau_c8.hostName, 
      sourceConnection.Tableau_c8.userName, 
      sourceConnection.Tableau_c8.password, 
      sourceConnection.Tableau_c8.apiLink,
      sourceConnection.Tableau_c8.apiVersion, 
      sourceConnection.Tableau_c8.apiUsername, 
      sourceConnection.Tableau_c8.apiPassword, 
      sourceConnection.Tableau_c8.executableWorkbooks, 
      sourceConnection.Tableau_c8.populateBIDict, 
      sourceConnection.Tableau_c8.publishExtraction,
      sourceConnection.Tableau_c8.metadataApi, 
      sourceConnection.Tableau_c8.metadataApiLink, 
      sourceConnection.Tableau_c8.contentUrl, 
      sourceConnection.Tableau_c8.disableThumbnailGeneration, 
      sourceConnection.Tableau_c8.loginType,
      sourceConnection.Tableau_c8.thumbnail,
      sourceConnection.Tableau_c8.thumbnailUserName,
      sourceConnection.Tableau_c8.thumbnailPassword,
    )
    cy.screenshot('Pass-test-0001')
  });
 
  it.skip('TC_0002 Add report type for Tableau', () => {
    logToGlobalFile('TC_0002 Add report type for Tableau')
    dashboard.openMenu()
    setting.openReportTypes()
    setting.searchconnection()
    setting.editReportType()
    //setting.reportUrl('Smoke Test Tableau','http://tableau-2021.zenoptics.com/','http://13.52.168.148/')
    setting.reportUrl(data.tableau.connectionName, data.tableau.reportURL, data.tableau.thumbnailURL)
    cy.screenshot('pass-test-0002')
  
  });

  it.skip("TC-0001_Run Complete Tableau Connection", () => {
    logToGlobalFile("TC-0001_Run Complete Tableau Connection")
    dashboard.openMenu()
    setting.openSourceConnectionDetails()
    //source.searchTableauRunComplete(sourceConnection.Tableau.connectionName)
    source.searchTableauRunComplete(sourceConnection.Tableau_c8.connectionName) //added
    cy.screenshot('pass-test-00011')
  });
  it.skip("TC-0001_Run  Tableau Connection", () => {
    logToGlobalFile("TC-0001_Run  Tableau Connection")
    dashboard.openMenu()
    setting.openSourceConnectionDetails()
    //source.searchTableauRun(sourceConnection.Tableau.connectionName)
    source.searchTableauRunComplete(sourceConnection.Tableau_c8.connectionName) //added
    cy.screenshot('pass-test-00001')
  });
  it("TC_0002 Add category to whatsNew", () => {
    logToGlobalFile("TC_0002 Add category to whatsNew")
    dashboard.openMenu();
    setting.categorizedReportPage()
   categorizedReportPage.selectcategorycheckbox(data.categorizedReportPage.categoryName)
   cy.screenshot('pass-test-002')
    
  });
  it("TC-001_Verify Links in the Side Panel are displayed properly (ZEN2022-TC-43)",{ tags: ["@smoke", "@sanity", "@regression"] },() => {
    logToGlobalFile("TC-001_Verify Links in the Side Panel are displayed properly (ZEN2022-TC-43)")
    dashboard.openMenu();
    dashboard.verifysideMenu(data.sidelinksText);
    cy.screenshot('Pass-test-001')
  });
  it("TC-002_1 Verify clicking on all the links on left nav bar expect Reports (ZEN2022-TC-44)", () => {
    logToGlobalFile("TC-002_1 Verify clicking on all the links on left nav bar expect Reports (ZEN2022-TC-44)")
    dashboard.openMenu();
    dashboard.verifySideMenuLinks(navbarlinks);
    cy.screenshot('pass-test-002_1')
  });
  //only testcase
  it("TC-002_2 Verify clicking Reports and its sub links (ZEN2022-TC-44)", () => {
    logToGlobalFile("TC-002_2 Verify clicking Reports and its sub links (ZEN2022-TC-44)",)
    dashboard.openMenu();
    dashboard.clickSideMenuLink(data.sidelinksText[1]);
    dashboard.verifySideMenuLinks(navbarreportlinks);
    cy.screenshot('pass-test-002_2')
  });
  it.only("TC-003 Verify performing search for a Report from Search bar (ZEN2022-TC-45)",{ tags: "@search" },() => {
    logToGlobalFile("TC-003 Verify performing search for a Report from Search bar (ZEN2022-TC-45)")
    gobal.gobalSearch_(data.searchReports.searchReport);
    cy.screenshot('Pass-test-003')
    //gobal.gobalSearchResult(data.searchReports.verifyReport);
  });

it.only("TC-004 Verify all the 3 views (List, Grid, Thumbnail) in My Reports, Favorites, What's New, Recently Viewed", () => {
  logToGlobalFile("TC-004 Verify all the 3 views (List, Grid, Thumbnail) in My Reports, Favorites, What's New, Recently Viewed")
  gobal.verifytheviews(data.sidelinks[1].Reports[1]);
  gobal.verifytheviews(data.sidelinks[1].Reports[2]);
  gobal.verifytheviews(data.sidelinks[1].Reports[3]);
  gobal.verifytheviews(data.sidelinks[1].Reports[4]);
  cy.screenshot('Pass-test-004')
});

//modified
it.only("TC-005 Verify manual upload thumbnail generation", () => {
  logToGlobalFile("TC-005 Verify manual upload thumbnail generation")
  dashboard.openMenu();
  setting.openThumbnailSettingTab();
  setting.thumbnailSearchReport(data.manualThumbnails.searchReport);
  setting.thumbnailVerifySearch(data.manualThumbnails.verifyReport);
  //setting.UploadThumbnail(data.changeCompanyLogo.uploadPath2, { force: true });
  const newPath = "C:\\Users\\Ankitha\\cypressautomation\\cypress\\fixtures\\companylogo.svg";//addded
  setting.UploadThumbnail(data.changeCompanyLogo.uploadPath2, { force: true });//added
  cy.screenshot('Passed-test-005')

});
it('TC-010 Verify if the fields in BI Dictionary are showing report using this field and authorized user.', () => {
  logToGlobalFile('TC-010 Verify if the fields in BI Dictionary are showing report using this field and authorized user.')
  dashboard.openMenu()
  setting.openBiDictionary()
  biDictionary.verifyAuthorizedUsersInBiDi(data.biDictionary.fieldName,data.biDictionary.reportName,data.biDictionary.expectedUserCount)
  cy.screenshot('pass-test-010')
})

it("TC-011 Verify if the fields in BI Glossary are showing report using this field", () => {
  logToGlobalFile("TC-011 Verify if the fields in BI Glossary are showing report using this field")
  dashboard.openMenu();
  setting.openBiDictionary()
  biDictionary.searchBiDictinaryField(data.biGlosarry.fieldName)
  biDictionary.approvefield()
  dashboard.openMenu()
  dashboard.clickSideMenuLink(data.sidelinks[4]);
  biGlossary.searchField(data.biGlosarry.fieldName)
  biGlossary.searchresult(data.biGlosarry.fieldName)
  cy.screenshot('pass-test-011')
  
 
})

it('TC_012 Launch Few Report & see there is no error',()=>{
  logToGlobalFile('TC_012 Launch Few Report & see there is no error')
    dashboard.openMenu()
    setting.openMyReports()
    dashboard.openMenu()
    setting.openReportFromMyReport(data.report.reportname)
    cy.screenshot('pass-test-012_1')

    cy.wait(2000)
    dashboard.openMenu()
    setting.navigateReportType()
    setting.openReportFromReportTypes(data.reportType.ConnectorType,data.reportType.ReportName)
    cy.screenshot('pass-test-012_2')
    
    dashboard.openMenu()
    setting.navigatecategories()
    dashboard.openMenu()
    cy.wait(4000)
    setting.openReportFromCatPage(data.category.category, data.category.subCategory1,  data.category.reportName)
    cy.screenshot('pass-test-012_3')
})

it('TC_013 Mark some reports as favorites and see if they show up on dashboard',()=>{
  logToGlobalFile('TC_013 Mark some reports as favorites and see if they show up on dashboard')
  dashboard.openMenu()
  setting.openMyReports()
  dashboard.openMenu()
  setting.openReportFromMyReport(data.report.reportname)
  report.toggleReportFavoritesOn('0')
  dashboard.verifyFavoriteInDashboard(data.report.reportname)
  cy.screenshot('pass-test-013_1')
  cy.wait(5000)
  //gobal.searchAndSelectFirstReport(data.reportType.ReportName)
  dashboard.openMenu()
  setting.navigateReportType()
  setting.openReportFromReportTypes(data.reportType.ConnectorType,data.reportType.ReportName)
  report.toggleReportFavoritesOn('1')
  dashboard.verifyFavoriteInDashboard(data.reportType.ReportName)
  cy.screenshot('pass-test-013_2')
 
})

it('TC-014_Verify Creating a WF and share the WF with some user and see if the email is sent out', () => {   
  logToGlobalFile('TC-014_Verify Creating a WF and share the WF with some user and see if the email is sent out')
  gobal.searchAndSelectFirstReport(data.workflow.searchreport)
  const createWorkflow_userCount = createWorkflow_multipleuser(data.workflow.createWF, data.workflow.user)
  report.share_wf(data.workflow.createWF)
  report.verify_sharecount(data.workflow.createWF,createWorkflow_userCount)
  cy.screenshot('pass-test-014')
});

it('TC-014_1 Share WF with a group and make sure that the group is getting decomposed to its members after saving.', () => {   
  logToGlobalFile('TC-014_1 Share WF with a group and make sure that the group is getting decomposed to its members after saving.')
  report.share_report_WF_group(data.workflow.workflowname3, data.workflow.usergroup)
  cy.screenshot('pass-test-014_1')
});


it.skip('TC-015 Subscribe to a report and add public and private comments and see if the subscribed user gets the email notification on public comment', () => {   
  //gobal.searchAndSelectFirstReport('UPN_URL_Report_1')  
  //modified
  logToGlobalFile('TC-015 Subscribe to a report and add public and private comments and see if the subscribed user gets the email notification on public comment')
  gobal.searchAndSelectFirstReport(data.subscribeReport.searchreport)

  report.toggleReportSubscribeOn()
  dashboard.clickProfile()
  dashboard.logout()
  logToGlobalFile('Login as Subscribed user');
  logins.loginUser2(Cypress.env(),Cypress.env('username1'), Cypress.env('password1'))
  //gobal.searchAndSelectFirstReport('UPN_URL_Report_1')
  //modified
  gobal.searchAndSelectFirstReport(data.subscribeReport.searchreport)

  report.reportPublicComment(data.subscribeReport.publicCommnet)
  //checkEmailSubscribe('New Comment posted on your subscribed report "' + 'UPN_URL_Report_1' + '"')
  //modified
  //checkEmailSubscribe('New Commentt posted on your subscribed report"' + '1 - Executive' + '"')
  cy.screenshot('pass-test-015')
})

it.skip('Tc-016 Launch the private comment from notification', () => { 
  logToGlobalFile('TC-016 Launch the private comment from notification')
  //gobal.searchAndSelectFirstReport('UPN_URL_Report_1') 
  //modified
  gobal.searchAndSelectFirstReport(data.subscribeReport.searchreport)

  //report.reportPrivateComment(data.user.userid,'testing private comment')
  //modified
  report.reportPrivateComment(Cypress.env('reg_username'),data.subscribeReport.privateCommeent)

  dashboard.clickProfile()
  dashboard.logout()
  logins.loginUser2(Cypress.env(),Cypress.env('reg_username'), Cypress.env('reg_password'))
  dashboard.clickProfile()
  dashboard.clickNotifications()
  dashboard.clickNotificationPrivateComment() 
  //report.verifyPrivateComment(data.user.firstLastName,'testing private comment')
  //modified
  report.verifyPrivateComment(data.subscribeReport.log_username, data.subscribeReport.privateCommeent)
  cy.screenshot('pass-test-016')
})

it('TC-018 Verify if you are able to see data on Report Stats for Zenoptics & Backend Stats', () => {
  logToGlobalFile('TC-018 Verify if you are able to see data on Report Stats for Zenoptics & Backend Stats')
  dashboard.openMenu()
  dashboard.clickSideMenuLink(data.sidelinksText[7])
  //reportStatistics.verifyreportStatistics()
  //reportStatistics.verifyreportusage()
  //reportStatistics.toggleBackendServerButton()
  //reportStatistics.verifybackendstatistics()
  logToGlobalFile('Verify Report is available under usage count');
  reportStatistics.verify_report()
  logToGlobalFile(' Verify searching and opening report');
  reportStatistics.searchreport()
  cy.screenshot('pass-test-018')

})

it('TC-019 Verfiy the multiview functionality', () => {
  logToGlobalFile('TC-019 Verfiy the multiview functionality');   
  dashboard.openMenu()
       setting.openMyReports()
       logToGlobalFile('Verify Report is opened in multiview');
       gobal.verifyMultiView(data.multiView.numberOfReports,2)
       cy.screenshot('pass-test-019')

 })
it('TC-020 Verify favoriting a report', () => {
  logToGlobalFile('TC-020 Verify favoriting a report');
   gobal.searchAndSelectFirstReport(data.favoriteReport.reportName)
   report.toggleReportsFavoritesOn()
   cy.screenshot('pass-test-020')
   
}) 
it('TC-022 Email report link and verify if it can be sent to the people added in recipients field', () => {
  logToGlobalFile('TC-022 Email report link and verify if it can be sent to the people added in recipients field');
  gobal.searchAndSelectFirstReport(data.emailReport["search&VerifyReport"])
  report.sendReportInEmail (data.emailReport.verifyInEmail)
  //checkEmail(data.emailReport.verifyInEmail)
  cy.screenshot('pass-test-022')
  
}) 

it('TC-023 Verify LDAP Connection', () => {
  dashboard.openMenu();
 logToGlobalFile(' Opening LDAP Configuration tab');
  setting.openLdapConfigurationTab()
  logToGlobalFile(` Configuring LDAP with URL: ${data.ladpConfiguration.ldapServerUrl1}`);
  ldap.ldapWithOneUrl(data.ladpConfiguration.ldapServerUrl1)
  logToGlobalFile(` Configuring LDAP Bind DN: ${data.ladpConfiguration.ldapbinddn}`);
  ldap.ldapBindDN(data.ladpConfiguration.ldapbinddn)
  logToGlobalFile(`Configuring LDAP Password DN: ${data.ladpConfiguration.ldappassword}`);
  ldap.ldapPassword(data.ladpConfiguration.ldappassword)
  logToGlobalFile(` Configuring LDAP User Base: ${data.ladpConfiguration.ldapuserbase}`);
  ldap.ldapUserBase(data.ladpConfiguration.ldapuserbase)
  logToGlobalFile(`Configuring LDAP Attributes: ${data.ladpConfiguration.ldapattributes}`);
  ldap.ldapAttributes(data.ladpConfiguration.ldapattributes)
  logToGlobalFile('Clicking on update button');
  ldap.updateButton()
  cy.screenshot('pass-test-023')

})
it('TC-024 Verify extractor scheduling functionality', () => {
  logToGlobalFile('TC-024 Verify extractor scheduling functionality');
  dashboard.openMenu()
  setting.openSourceConnectionDetails()
  //source.schedulerun(sourceConnection.Tableau.connectionName)
  source.schedulerun(sourceConnection.Tableau_c8.connectionName)
  cy.screenshot('pass-test-024')

});
it('TC-026 Tableau report launching', () => {
  dashboard.openMenu()
  setting.navigateReportType()
  report.openTableauReport(data.Tableau_ReportType.reportType, data.Tableau_ReportType.reportname)
  cy.screenshot('pass-test-026')

})
it("TC-027 Verify Report types page", () => {
  logToGlobalFile('TC-027 Verify Report types page');
  dashboard.openMenu();
dashboard.clickSideMenuLink(data.reportType.page);
dashboard.openMenu();
reportType.ConnectorType(data.reportType.ConnectorType);
reportType.sortOrder(data.reportType.sortOrder);
cy.screenshot('pass-test-027')
});
it("TC-028 Verify Categorized Reports page", () => {
  logToGlobalFile('TC-028 Verify Categorized Reports page');
  dashboard.openMenu();
setting.categorizedReportPage()
logToGlobalFile(`Select category: ${data.categorizedReportPage.categoryName}`);
categorizedReportPage.selectCategory(data.categorizedReportPage.categoryName)
cy.screenshot('pass-test-028')
});
it('TC_029 Verify Compnay Logo Uploading',()=>{
  logToGlobalFile('TC_029 Verify Compnay Logo Uploading');
  setting.companyLogo()
  cy.screenshot('pass-test-029')
})

it.skip('TC-031 Verify the favorited reports are displayed as "Favorites" in the search result', () => {
  logToGlobalFile('TC-031 Verify the favorited reports are displayed as "Favorites" in the search result');
  logToGlobalFile(`Verify favourite icon in Quick search for report ${data.favoriteReport.reportName}`);
  gobal.verifyFavIconInSearch(data.favoriteReport.reportName)
  cy.screenshot('pass-test-031')
})
it.skip('TC-032 Verify Updating a workflow including updating shared users , adding reports,removing reports and changing the order of reports', () => {
  try{
    logToGlobalFile('TC-032 Verify Updating a workflow including updating shared users , adding reports,removing reports and changing the order of reports');
  report.add_report_WF(data.workflow.workflowname, data.workflow.add_WF1, data.workflow.add_WF2)
  cy.screenshot('pass-test-032')
  

  }
  catch (error){
    logError(error, 'TC-00001_Verify creating a new Tableau Connection');
      throw error;
  }
  
})

it('TC-032_2 Verify Updating a workflow including updating shared users , adding reports,removing reports and changing the order of reports', () => {
  try{
    
  logToGlobalFile('TC-032_1 Verify Updating a workflow including updating shared users , adding reports,removing reports and changing the order of reports')
  report.remove_report_WF(data.workflow.workflowname1, data.workflow.user)
  cy.screenshot('pass-test-032_2')
  

  }
  catch (error){
    logError(error, 'TC-00001_Verify creating a new Tableau Connection');
      throw error;
  }
  
})

it('TC-032_3 Verify Updating a workflow including updating shared users , adding reports,removing reports and changing the order of reports', () => {
  try{
    
  logToGlobalFile('TC-032_2 Verify Updating a workflow including updating shared users , adding reports,removing reports and changing the order of reports');
  report.share_report_WF_user(data.workflow.workflowname3, data.workflow.user)
  cy.screenshot('pass-test-032_3')


  }
  catch (error){
    logError(error, 'TC-00001_Verify creating a new Tableau Connection');
      throw error;
  }
  
})


it('TC-034 Switching between classic and modern dashboard', () => {
 logToGlobalFile('TC-034 Switching between classic and modern dashboard')
  dashboard.clickProfile()
  dashboard.dashboardSetting()
  logToGlobalFile('Toggle dashboard style');
  dashboard.toggleDashboardStyle()
  cy.screenshot('pass-test-034')
 

});

it('TC-035 Verify the Experimental flags for dashboard personalization', () => {
  logToGlobalFile('TC-035 Verify the Experimental flags for dashboard personalization');
  dashboard.openMenu()
  setting.openApplicationSettings()
  setting.experimentalflag_classic()
  dashboard.clickHomeIcon()
  cy.screenshot('pass-test-035_1')
  //modern
  dashboard.openMenu()
  setting.openApplicationSettings()
  setting.experimentalflag_modern()
  dashboard.clickHomeIcon()
  cy.screenshot('pass-test-035_2')
  //disable
  dashboard.openMenu()
  setting.openApplicationSettings()
  setting.styleswitch_disable()
  //cy.wait(10000)
  dashboard.clickProfile()
  dashboard.dashboardSetting()
  //cy.get(".visual-style-radio.ng-star-inserted").should('not.exist')
  cy.get(".dashboard-setting__header--close").click()
  cy.screenshot('pass-test-035_3')
  //enable
  dashboard.openMenu()
  setting.openApplicationSettings()
  setting.styleswitch_enable()
  //cy.wait(10000)
  dashboard.clickProfile()
  dashboard.dashboardSetting()
  cy.get(".visual-style-radio.ng-star-inserted").should('exist')
  cy.get(".dashboard-setting__header--close").click()
  cy.screenshot('pass-test-035_4')
})

it.skip('TC-036 Verify the thumbnail attributes across different widgets in the Dashboard', () => {
  logToGlobalFile('TC-036 Verify the thumbnail attributes across different widgets in the Dashboard');
  report.openDashReport()
  report.toggleReportsFavoritesOn()
  report.reportAbout()
  report.reportdetails()
  dashboard.clickHomeIcon()
  report.openDashReportinfo()
  dashboard.openMenu()
  setting.openApplicationSettings()
  setting.clickcustomAttribute_on()
  dashboard.clickHomeIcon()
  report.openDashboardinfo_details()
  dashboard.openMenu()
  setting.openApplicationSettings()
  setting.clickcustomAttribute_off()
  dashboard.clickHomeIcon()
  //report.openDashboardinfo_details_off()


  

})

it('TC-038 Verify Pinning and Unpinning workflow from dashboard', () => {
  logToGlobalFile('TC-038 Verify Pinning and Unpinning workflow from dashboard')
  dashboard.clickProfile();
  dashboard.dashboardSetting();
  dashboard.toggleModernDashboard();
  dashboard.pinUnpinWFFromDash()
  cy.screenshot('pass-test-038')
  
  
})

it('TC-039 Verify Pinning and Unpinning workflow from workflow page', () => {
  logToGlobalFile('TC-039 Verify Pinning and Unpinning workflow from workflow page')
  dashboard.clickProfile();
  dashboard.dashboardSetting();
  dashboard.toggleModernDashboard();
  dashboard.pinUnpinCatsFromWF()
  cy.screenshot('pass-test-039')
})

const reportSections = [1, 2, 3, 4];
const dashboardSections = [2, 3, 4, 5]

it("TC-042_2 Verify back button and refresh works in below pages in the application ", () => {
  logToGlobalFile("TC-042_2 Verify back button and refresh works in below pages in the application")  
  cy.wait(5000)
    dashboard.navigateToDashboardSections(dashboardSections)
    cy.screenshot('pass-test-042_2')
   
  });
  
it("TC-042_3 Verify back button and refresh works in below pages in the application ", () => {
  logToGlobalFile('TC-042_3 Verify back button and refresh works in below pages in the application');  
  cy.wait(5000)
    dashboard.openMenu()
    setting.openBiDictionaryReload() 
    cy.screenshot('pass-test-042_3')
    
  });
  
it("TC-042_4 Verify back button and refresh works in below pages in the application ", () => {
  logToGlobalFile('TC-042_4 Verify back button and refresh works in below pages in the application');  
  cy.wait(5000)
    report.openRandomWorkflow_Reload(data.workflow.workflowname)
    cy.screenshot('pass-test-042_4')
    
  });
  
it("TC-042_5 Verify back button and refresh works in below pages in the application ", () => {
  logToGlobalFile('TC-042_5 Verify back button and refresh works in below pages in the application');  
  cy.wait(5000)
    gobal.advanceSearch(data.searchReports.advance_search)
    cy.screenshot('pass-test-042_5')
  });
  
it("TC-046 Verify the pagination is maintained when Back button is cliked in BI Dictionary", () => {
  logToGlobalFile('TC-046 Verify the pagination is maintained when Back button is cliked in BI Dictionary');  
  //Users
    dashboard.openMenu()
    setting.openUsersettings()
    setting.clickOnNextUser()
    cy.screenshot('pass-test-046_1')
    //source Connection Details
    dashboard.openMenu()
    setting.openSourceConnectionDetails()
    cy.screenshot('pass-test-046_2')
    //setting.clickOnNextSourceConnection()
    //SourceConncetionDetails-> Job History
    //dashboard.openMenu()
    //setting.openSourceConnectionDetails()
    //setting.openJobHistory()
    //Maintain Report
    dashboard.openMenu()
    //setting.openMaintanReport()
    setting.openMaintanReport_srv15()
    setting.clickOnNextMaintainReports()
    cy.screenshot('pass-test-046_3')
    //Report Type
    dashboard.openMenu()
    setting.openReportTypes()
    setting.clickOnNextReportTypes()
    cy.screenshot('pass-test-046_4')
  });

it('TC-050 Verify Creating a Newly created User and verify accessing the application and dashboard', () => {
  logToGlobalFile('TC-050 Verify Creating a Newly created User and verify accessing the application and dashboard');  
  const { email, firstName, lastName,password} = randomData();
    dashboard.openMenu()
    setting.openUsersettings()
    setting.createActiveUser(firstName,password,lastName,email)
    dashboard.clickProfile()
    dashboard.logout()
    logins.loginUser2(Cypress.env(),firstName,password)
    dashboard.openMenu()
    cy.screenshot('pass-test-050')
    
  });

it('TC-051 Verify pinning categories from dashboard page', () => {
  logToGlobalFile('TC-038 Verify pinning/unpinning categories from dashboard page');
  dashboard.clickProfile();
  dashboard.dashboardSetting();
  dashboard.toggleModernDashboard();
  dashboard.pinCatFromDash()
  cy.screenshot('pass-test-051')
  
})

it('TC-053 Verify pinning and unpinning sub categories from dashboard', () => {
  logToGlobalFile('TC-038 Verify pinning/unpinning categories from dashboard page');
  dashboard.clickProfile();
  dashboard.dashboardSetting();
  dashboard.toggleModernDashboard();
  dashboard.pinUnpinSubCatFromDash()
  cy.screenshot('pass-test-053')
  
})

it('TC-052 Verify unpinning categories from dashboard page', () => {
  logToGlobalFile('TC-038 Verify pinning/unpinning categories from dashboard page');
  dashboard.clickProfile();
  dashboard.dashboardSetting();
  dashboard.toggleModernDashboard();
  dashboard.unpinCatFromDash()
  cy.screenshot('pass-test-052')
})


it('TC-054 Verify pinning/unpinning categories from categories page', () => {
  logToGlobalFile('TC-054 Verify pinning/unpinning categories from categories page')
    dashboard.clickProfile();
    dashboard.dashboardSetting();
    dashboard.toggleModernDashboard();
    dashboard.pinUnpinCatsFromCatPage()
    cy.screenshot('pass-test-054')
 
  })

it('TC-055 Verify pinning/unpinning subcategories from subcategories page', () => {
  logToGlobalFile('TC-055 Verify pinning/unpinning subcategories from subcategories page')
    dashboard.clickProfile();
    dashboard.dashboardSetting();
    dashboard.toggleModernDashboard();
    dashboard.pinUnpinCatsFromSubCatPage()
    cy.screenshot('pass-test-055')
 
  })

it('TC-056 Verify the Single view with Multiple reports open', () => {
  logToGlobalFile('TC-056 Verify the Single view with Multiple reports open');  
  dashboard.openMenu()
    setting.openMyReports()
    gobal.verifySingleView(data.multiView.numberOfReports,2)
    cy.screenshot('pass-test-056')
    
  });
  
it('TC-057 Verify the Split view with Multiple reports open', () => {
  logToGlobalFile('TC-057 Verify the Split view with Multiple reports open');   
  dashboard.openMenu()
    setting.openMyReports()
    gobal.verifySplitView(data.multiView.numberOfReports,2)
    cy.screenshot('pass-testt-057')
    
  });

it('TC-058 Verify scheduling auto-refresh options in split View', () => {
  logToGlobalFile('TC-058 Verify scheduling auto-refresh options in split View');  
  dashboard.openMenu()
    setting.openMyReports()
    gobal.verifySplitViewSchedule(data.multiView.numberOfReports,2)
    cy.screenshot('pass-test-058')
  });

it('TC-059 Verify scheduling auto-refresh options in Single View', () => {
  logToGlobalFile('TC-059 Verify scheduling auto-refresh options in Single View');  
  dashboard.openMenu()
    setting.openMyReports()
    gobal.verifySingleViewSchedule(data.multiView.numberOfReports,2)
    cy.screenshot('pass-test-059')
      
  });

it('TC-60 Verify scheduling auto-refresh in Multi View', () => {
  logToGlobalFile('C-60 Verify scheduling auto-refresh in Multi View');  
  dashboard.openMenu()
    setting.openMyReports()
    gobal.verifyMultiViewSchedule(2,3)
    cy.screenshot('pass-test-060')
    
  });
it('TC-061 Verify the Rating, View Count and report info are displayed correctly for the search results in Quick Search Page', () => {
  logToGlobalFile('TC-061 Verify the Rating, View Count and report info are displayed correctly for the search results in Quick Search Page');  
  gobal.gobalSearch(data.searchReports.advance_search)
    gobal.verifytheViewQuickSearch()
    cy.screenshot('pass-test-061')
    
  });
  
it('TC-062 Verify the Rating, View Count and report info are displayed correctly for the search results in Advanced Search Page', () => {
  logToGlobalFile('TC-062 Verify the Rating, View Count and report info are displayed correctly for the search results in Advanced Search Page');  
  gobal.advanceSearch(data.searchReports.advance_search)
    gobal.verifytheViewAdvanceSearch()
    cy.screenshot('pass-test-062')
   
  });

it('TC-063 Verify new workflow created and delete successfully', () => {   
  logToGlobalFile('TC-063 Verify new workflow created and delete successfully');  
  report.getRandomReport() 
    gobal.searchAndSelectFirstReport(data.workflow.searchreport2)
    report.createWorkflow(data.workflow.new_WF,data.workflow.user)
    report.getRandomWorkflow()// added
    report.deleteWorkflow_1(data.workflow.new_WF)  
    cy.screenshot('pass-test-063')
});

it('TC-064 Tableau Connector Delete', () => {
logToGlobalFile('TC-064 Tableau Connector Delete');
  dashboard.openMenu()
  setting.openSourceConnectionDetails()
  source.searchTableauConnection(sourceConnection.Tableau_c8.connectionName)
  source.deleteTableauConnection()
  cy.screenshot('pass-test-064')
 
});


  
afterEach(() => {
  if (Cypress.mocha.currentTest) {
    logScreenshots();
  }
});


after(() => {
   if (Cypress.mocha.currentTest) {
    logScreenshots();
    logToGlobalFile();
  }
  
});


})
